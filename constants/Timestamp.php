<?php

namespace MVF\Servicer\Constants;

class Timestamp
{
    public const START_OF_YEAR_2000 = 946684800;
}

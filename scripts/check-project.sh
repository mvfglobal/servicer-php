#!/bin/sh

PURPLE='\033[0;35m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NO_COLOUR='\033[0m'

EXPECTED_PROJECT_NAME="${GREEN}servicer-php${PURPLE}"
ACTUAL_PROJECT_NAME="${RED}${PWD##*/}${PURPLE}"
MESSAGE="${PURPLE}Project directory name must be '${EXPECTED_PROJECT_NAME}' but is '${ACTUAL_PROJECT_NAME}', please rename it.${NO_COLOUR}"

# Ensure directory name is correct, this is necessary for some project scripts to work correctly
echo "${PWD##*/}" | grep -E "^servicer-php$" > /dev/null || (echo "${MESSAGE}" && exit 1)

#!/bin/bash

if [ 'Linux' == "$(uname)" ]; then
  OS_UID=$(id -u)
  OS_GID=$(id -g)
else
  OS_UID=1000
  OS_GID=1000
fi

if [ -z "${GLOBAL_GITHUB_ACCESS_TOKEN}" ]; then
    read -p '
WARNING: GLOBAL_GITHUB_ACCESS_TOKEN is not set in your rc file (either .bashrc, .zshrc, etc.) fallback to cli

Setup requires Github Personal Access token:
1: Open https://github.com/settings/tokens
2: Create a token with no scopes and
3: Enter token: ' GITHUB_PAT
else
    GITHUB_PAT="${GLOBAL_GITHUB_ACCESS_TOKEN}"
fi

echo "export BUILD_GITHUB_ACCESS_TOKEN=${GITHUB_PAT}
export OS_UID=${OS_UID}
export OS_GID=${OS_GID}\
" > ./env/docker

<?php

namespace MVF\Servicer;

use GuzzleHttp\Utils;
use MVF\Servicer\Contracts\EventHandler;
use MVF\Servicer\Contracts\EventTransformer;
use MVF\Servicer\Contracts\Logger;
use MVF\Servicer\Contracts\QueueTransformer;
use MVF\Servicer\Events\GenericEvent;
use MVF\Servicer\BaseEvent\BaseEvent;
use MVF\Servicer\Exceptions\EventTransformerMustReturnGenericEvent;
use MVF\Servicer\Exceptions\ObjectMustImplementEventHandler;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionClass;
use function Functional\each;

class Receiver
{
    private ContainerInterface $container;
    private Logger $logger;
    private string $queue;

    public function __construct(ContainerInterface $container, Logger $logger, string $queue)
    {
        $this->container = $container;
        $this->logger = $logger;
        $this->queue = $queue;
    }

    /**
     * @throws ContainerExceptionInterface
     */
    public function handle(array $headers, array $body, ?string $queueOverride = null)
    {
        $event = $headers['event'];

        try {
            $key = sprintf('%s->%s', $queueOverride ?? $this->queue, $event);
            $handler = $this->container->get($key);
        } catch (NotFoundExceptionInterface $exception) {
            $this->logger->warn('ignoring request for an event that has no handler defined', [
                'headers' => $headers,
                'body' => $body,
            ]);
            return;
        }

        $runHandlerFn = $this->runHandler($event, $headers, $body);
        if (is_array($handler)) {
            each($handler, $runHandlerFn);
        } elseif ($handler instanceof Handlers) {
            each($handler->getHandlers(), $runHandlerFn);
        } else {
            $runHandlerFn($handler);
        }
    }

    private function runHandler($event, array $headers, $body): callable
    {
        return function ($handler, $key = null) use ($event, $headers, $body) {
            $headers = $this->extendIdentifier($headers, $key);

            if ($handler instanceof EventHandlerDecorator) {
                $action = $handler->getName();
            } else {
                $reflect = new ReflectionClass($handler);
                $action = $reflect->getShortName();
            }

            if ($handler instanceof EventHandler) {
                $message = 'Payload: ' . Utils::jsonEncode(['headers' => $headers, 'body' => $body]);
                $meta = [
                    'event' => $event,
                    'action' => $action,
                    'payload' => [
                        'headers' => $headers,
                        'body' => $body,
                    ],
                ];

                $this->logger->info($message, array_replace($meta, ['state' => 'STARTED']));

                $payload = [
                    'headers' => $headers,
                    'body' => $body,
                ];

                $result = $handler->consume($payload);
                if ($result instanceof EventTransformer) {
                    $this->logger->info($message, array_replace($meta, ['state' => 'REDIRECTED']));
                    $this->redirectEvent($result, $payload);
                } else {
                    $this->logger->info($message, array_replace($meta, ['state' => 'COMPLETED']));
                }
            } else {
                throw new ObjectMustImplementEventHandler(get_class($handler));
            }
        };
    }

    /**
     * @throws ContainerExceptionInterface
     */
    private function redirectEvent(EventTransformer $transformPayload, array $previousPayload)
    {
        $queueOverride = $transformPayload instanceof QueueTransformer
            ? $transformPayload->getQueueOverride()
            : null;

        $event = $transformPayload($previousPayload);
        if (!($event instanceof GenericEvent)) {
            throw new EventTransformerMustReturnGenericEvent(get_class($transformPayload), get_class($event));
        }

        $nextHeaders = $event->getGenericHeaders()->toArray();
        if ($event->getGenericHeaders()->getCreatedAt()->timestamp === 0) {
            $previousEvent = BaseEvent::validateAndParse($previousPayload);
            $nextHeaders['createdAt'] = $previousEvent->getHeaders()->getCreatedAt()->timestamp;
        }

        $this->handle(
            $nextHeaders,
            $event->getGenericBody()->toArray(),
            $queueOverride,
        );
    }

    private function extendIdentifier(array $headers, $key = null): array
    {
        if (!isset($headers['identifier']) || !isset($key)) {
            return $headers;
        }

        $headers['identifier'] = $headers['identifier'] . ':' . $key;

        return $headers;
    }
}

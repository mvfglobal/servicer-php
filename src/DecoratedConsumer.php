<?php

namespace MVF\Servicer;

use MVF\Servicer\Contracts\EventHandler;

class DecoratedConsumer extends EventHandlerDecorator
{
    /**
     * @param EventHandlerDecorator[] $decorators
     */
    public function __construct(array $decorators, EventHandler $eventHandler)
    {
        $current = $eventHandler;
        foreach (array_reverse($decorators) as $decorator) {
            $decorator->setNext($current);
            $current = $decorator;
        }

        $this->decorator = $current;
    }

    public function consume(array $payload)
    {
        return $this->decorator->consume($payload);
    }
}

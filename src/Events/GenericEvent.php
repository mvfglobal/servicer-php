<?php

namespace MVF\Servicer\Events;

use GuzzleHttp\Utils;
use MVF\Servicer\Contracts\ArraySerializable;
use MVF\Servicer\Contracts\ValidatedObject;

abstract class GenericEvent implements ArraySerializable, ValidatedObject
{
    protected GenericHeaders $headers;
    protected GenericBody $body;

    public function getGenericHeaders(): GenericHeaders
    {
        return $this->headers;
    }

    public function getGenericBody(): GenericBody
    {
        return $this->body;
    }

    public function getHeadersAsJson(): string
    {
        $headers = $this->headers->toArray();
        unset($headers['createdAt']);

        return Utils::jsonEncode($headers);
    }

    public function getBodyAsJson(): string
    {
        return Utils::jsonEncode($this->body->toArray());
    }

    public function toArray(): array
    {
        return [
            'headers' => $this->headers->toArray(),
            'body' => $this->body->toArray(),
        ];
    }
}

<?php

namespace MVF\Servicer\Events;

use Carbon\Carbon;
use MVF\Servicer\Contracts\ArraySerializable;
use MVF\Servicer\Contracts\ValidatedObject;

abstract class GenericHeaders implements ValidatedObject, ArraySerializable
{
    protected string $event;
    protected string $version;
    protected string $identifier;
    protected int $createdAt;

    public function __construct(string $event, string $version, string $identifier, int $createdAt = 0)
    {
        $this->event = $event;
        $this->version = $version;
        $this->identifier = $identifier;
        $this->createdAt = $createdAt;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getCreatedAt(): Carbon
    {
        return Carbon::createFromTimestamp($this->createdAt);
    }
}

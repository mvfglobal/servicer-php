<?php

namespace MVF\Servicer\Events;

use MVF\Servicer\Contracts\ArraySerializable;
use MVF\Servicer\Contracts\ValidatedObject;

abstract class GenericBody implements ValidatedObject, ArraySerializable
{
}

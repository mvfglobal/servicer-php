<?php

namespace MVF\Servicer\Events\DefaultValidationRules\Valitron;

class ValidatorRule
{
    const REQUIRED = 'required';
    const REQUIRED_WITH = 'requiredWith';
    const REQUIRED_WITHOUT = 'requiredWithout';
    const EQUALS = 'equals';
    const DIFFERENT = 'different';
    const ACCEPTED = 'accepted';
    const NUMERIC = 'numeric';
    const INTEGER = 'integer';
    const BOOLEAN = 'boolean';
    const LENGTH = 'length';
    const LENGTH_BETWEEN = 'lengthBetween';
    const LENGTH_MIN = 'lengthMin';
    const LENGTH_MAX = 'lengthMax';
    const MIN = 'min';
    const MAX = 'max';
    const LIST_CONTAINS = 'listContains';
    const IN = 'in';
    const NOT_IN = 'notIn';
    const IP = 'ip';
    const IPV4 = 'ipv4';
    const IPV6 = 'ipv6';
    const EMAIL = 'email';
    const EMAIL_DNS = 'emailDNS';
    const URL = 'url';
    const URL_ACTIVE = 'urlActive';
    const ALPHA = 'alpha';
    const ALPHA_NUM = 'alphaNum';
    const ASCII = 'ascii';
    const SLUG = 'slug';
    const REGEX = 'regex';
    const DATE = 'date';
    const DATE_FORMAT = 'dateFormat';
    const DATE_BEFORE = 'dateBefore';
    const DATE_AFTER = 'dateAfter';
    const CONTAINS = 'contains';
    const SUBSET = 'subset';
    const CONTAINS_UNIQUE = 'containsUnique';
    const CREDIT_CARD = 'creditCard';
    const INSTANCE_OF = 'instanceOf';
    const OPTIONAL = 'optional';
    const ARRAY_HAS_KEYS = 'arrayHasKeys';
    const ARRAY = 'array';
}

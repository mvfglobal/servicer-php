<?php

namespace MVF\Servicer\Events\DefaultValidationRules\Valitron;

use MVF\Servicer\Constants\Timestamp;

trait EventHeaderMapValidationRules
{
    protected static function defaultValidationRules(string ...$eventName): array
    {
        return [
            'headers.event' => array_merge(
                [ValidatorRule::REQUIRED],
                !empty($eventName) ? [[ValidatorRule::IN, $eventName]]: [],
            ),
            'headers.version' => [
                ValidatorRule::REQUIRED,
                [ValidatorRule::LENGTH_MIN, 1],
            ],
            'headers.identifier' => [
                ValidatorRule::REQUIRED,
                [ValidatorRule::LENGTH_MIN, 1],
            ],
            'headers.createdAt' => [
                ValidatorRule::INTEGER,
                [ValidatorRule::MIN, Timestamp::START_OF_YEAR_2000],
            ],
        ];
    }
}

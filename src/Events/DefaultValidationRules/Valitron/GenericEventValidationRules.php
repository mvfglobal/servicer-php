<?php

namespace MVF\Servicer\Events\DefaultValidationRules\Valitron;

trait GenericEventValidationRules
{
    protected static function defaultValidationRules(): array
    {
        return [
            ValidatorRule::REQUIRED => ['headers', 'body'],
            ValidatorRule::ARRAY => ['headers', 'body'],
        ];
    }
}

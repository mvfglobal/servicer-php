<?php

namespace MVF\Servicer\Events\DefaultValidationRules\Valitron;

use MVF\Servicer\Constants\Timestamp;

trait EventHeaderValidationRules
{
    protected static function defaultValidationRules(string ...$eventName): array
    {
        $rules = [
            ValidatorRule::REQUIRED => [
                'headers.event',
                'headers.version',
                'headers.identifier',
                'headers.createdAt'
            ],
            ValidatorRule::LENGTH_MIN => [
                ['headers.version', 1],
                ['headers.identifier', 1],
            ],
            ValidatorRule::INTEGER => [
                'headers.createdAt',
            ],
            ValidatorRule::MIN => [
                ['headers.createdAt', Timestamp::START_OF_YEAR_2000],
            ],
        ];

        if (!empty($eventName)) {
            $rules[ValidatorRule::IN] = [['headers.event', $eventName]];
        }

        return $rules;
    }
}

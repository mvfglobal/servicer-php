<?php

namespace MVF\Servicer\Events\DefaultValidationRules\Valitron;

trait GenericEventMapValidationRules
{
    protected static function defaultValidationRules(): array
    {
        return [
            'headers' => [ValidatorRule::REQUIRED, ValidatorRule::ARRAY],
            'body' => [ValidatorRule::REQUIRED, ValidatorRule::ARRAY],
        ];
    }
}

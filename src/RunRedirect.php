<?php

namespace MVF\Servicer;

use MVF\Servicer\Contracts\EventHandler;
use MVF\Servicer\Contracts\EventTransformer;
use ReflectionClass;

class RunRedirect extends EventHandlerDecorator implements EventHandler
{
    private EventTransformer $transformPayload;

    public function __construct(EventTransformer $transformPayload)
    {
        $this->transformPayload = $transformPayload;
    }

    public function getName(): string
    {
        $reflect = new ReflectionClass($this->transformPayload);

        return sprintf('Transform:%s', $reflect->getShortName());
    }

    public function consume(array $payload): EventTransformer
    {
        return $this->transformPayload;
    }
}

<?php

namespace MVF\Servicer;

use MVF\Servicer\Contracts\EventHandler;
use ReflectionClass;

abstract class EventHandlerDecorator implements EventHandler
{
    protected EventHandler $decorator;

    public function setNext(EventHandler $decorator): void
    {
        $this->decorator = $decorator;
    }

    public function getName(): string
    {
        if ($this->decorator instanceof EventHandlerDecorator) {
            return $this->decorator->getName();
        }

        $reflect = new ReflectionClass($this->decorator);

        return $reflect->getShortName();
    }
}

<?php

namespace MVF\Servicer;

/**
 * @deprecated This class was moved to \MVF\Servicer\Controllers\Slim\EventsController namespace
 */
class EventsController extends \MVF\Servicer\Controllers\Slim\EventsController
{
}

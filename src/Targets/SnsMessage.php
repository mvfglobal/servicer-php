<?php

namespace MVF\Servicer\Targets;

use MVF\Servicer\ArraySerializer;
use MVF\Servicer\Contracts\ArraySerializable;
use MVF\Servicer\Exceptions\TemplateNotFoundInTheEnvironment;

class SnsMessage implements ArraySerializable
{
    use ArraySerializer;

    private string $arn;

    /**
     * SnsMessage constructor.
     *
     * @param string $arn Name of the topic or full topic arn if $env parameter is empty
     * @param string|null $env Variable to be used to load the template of sns arn
     */
    public function __construct(string $arn, string $env = null)
    {
        if (isset($env)) {
            $template =  getenv($env);

            if (empty($template)) {
                throw new TemplateNotFoundInTheEnvironment($env);
            }

            $this->arn = sprintf($template, $arn);
        } else {
            $this->arn = $arn;
        }
    }

    /**
     * Returns the type of the provider.
     *
     * @return string
     */
    public function getProvider(): string
    {
        return 'SNS';
    }

    /**
     * Returns the arn of the SNS topic.
     *
     * @return string
     */
    public function getArn(): string
    {
        return $this->arn;
    }
}

<?php

namespace MVF\Servicer\Targets;

use MVF\Servicer\ArraySerializer;
use MVF\Servicer\Contracts\ArraySerializable;
use MVF\Servicer\Exceptions\TemplateNotFoundInTheEnvironment;

class SqsMessage implements ArraySerializable
{
    use ArraySerializer;

    private string $url;

    /**
     * SqsMessage constructor.
     *
     * @param string $name Name of the queue or full queue url if $env parameter is empty
     * @param string|null $env Variable to be used to load the template of the sqs url
     */
    public function __construct(string $name, string $env = null)
    {
        if (isset($env)) {
            $template =  getenv($env);

            if (empty($template)) {
                throw new TemplateNotFoundInTheEnvironment($env);
            }

            $this->url = sprintf($template, $name);
        } else {
            $this->url = $name;
        }
    }

    /**
     * Returns the type of the provider.
     */
    public function getProvider(): string
    {
        return 'SQS';
    }

    /**
     * Get the SQS queue url.
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}

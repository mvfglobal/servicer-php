<?php

namespace MVF\Servicer;

use MVF\Servicer\Contracts\Action;
use MVF\Servicer\Contracts\EventHandler;
use ReflectionClass;

class RunAction extends EventHandlerDecorator implements EventHandler
{
    private Action $action;
    /**
     * @var callable|null
     */
    private $transformPayload;

    public function __construct(Action $action, ?callable $transformPayload = null)
    {
        $this->action = $action;
        $this->transformPayload = $transformPayload;
    }

    public function getName(): string
    {
        $reflect = new ReflectionClass($this->action);

        return $reflect->getShortName();
    }

    public function consume(array $payload): void
    {
        $this->action->handler($this->transformPayload ? ($this->transformPayload)($payload) : $payload);
    }
}

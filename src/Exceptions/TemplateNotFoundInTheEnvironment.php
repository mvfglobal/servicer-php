<?php

namespace MVF\Servicer\Exceptions;

use RuntimeException;

class TemplateNotFoundInTheEnvironment extends RuntimeException
{
    public function __construct(string $environmentVariable)
    {
        $docs = 'https://bitbucket.org/mvfglobal/servicer-php/src/master/docs/exceptions/TemplateNotFoundInTheEnvironment.md';
        parent::__construct("Environment variable '$environmentVariable' is not defined, read documentation $docs");
    }
}

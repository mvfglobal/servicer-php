<?php

namespace MVF\Servicer\Exceptions;

use MVF\Servicer\Contracts\EventHandler;
use RuntimeException;

class ObjectMustImplementEventHandler extends RuntimeException
{
    public function __construct(string $environmentVariable)
    {
        $className = EventHandler::class;
        $docs = 'https://bitbucket.org/mvfglobal/servicer-php/src/master/docs/exceptions/ObjectMustImplementEventHandler.md';
        parent::__construct("Object '$environmentVariable' must implement '$className', read documentation $docs");
    }
}

<?php

namespace MVF\Servicer\Exceptions;

use MVF\Servicer\Contracts\EventHandler;
use RuntimeException;

class EventTransformerMustReturnGenericEvent extends RuntimeException
{
    public function __construct(string $eventTransformer, string $transformedObject)
    {
        $docs = 'https://bitbucket.org/mvfglobal/servicer-php/src/master/docs/exceptions/EventTransformerMustReturnGenericEvent.md';
        parent::__construct("Event transformer '$eventTransformer' returned '$transformedObject' object which does not implement GenericEvent, read documentation $docs");
    }
}

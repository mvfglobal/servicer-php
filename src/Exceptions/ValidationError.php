<?php

namespace MVF\Servicer\Exceptions;

use MVF\Servicer\Contracts\EventHandler;
use RuntimeException;

class ValidationError extends RuntimeException
{
    private array $errors;

    public function __construct(array $errors)
    {
        $this->errors = $errors;

        parent::__construct(
            sprintf('Event payload failed validation rules for [%s]', implode(', ', array_keys($this->errors))),
        );
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}

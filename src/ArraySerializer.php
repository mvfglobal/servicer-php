<?php

namespace MVF\Servicer;

use MVF\Servicer\Contracts\ArraySerializable;
use function Functional\map;

trait ArraySerializer
{
    /**
     * Constructs array of object's attributes and values and transforms attributes to snake case.
     */
    public function toArray(): array
    {
        $attributes = get_object_vars($this);
        $keys = array_keys($attributes);
        $values = map(array_values($attributes), $this->transformToPayload());

        return array_combine($keys, $values);
    }

    /**
     * Higher order function that converts attribute values to either values or associative arrays.
     */
    private function transformToPayload(): callable
    {
        return function ($value) {
            if ($value instanceof ArraySerializable) {
                return $value->toArray();
            }

            if (is_array($value) === true) {
                return map($value, $this->transformToPayload());
            }

            if (is_object($value) === true) {
                $message = 'Invalid object attribute found, toPayload() can only transform objects that implement ' .
                    ArraySerializable::class .
                    '.';

                throw new \Exception($message);
            }

            return $value;
        };
    }
}

<?php

namespace MVF\Servicer\BaseEvent;

use MVF\Servicer\Events\DefaultValidationRules\Valitron\GenericEventValidationRules;
use MVF\Servicer\Events\GenericEvent;
use MVF\Servicer\Exceptions\ValidationError;

class BaseEvent extends GenericEvent
{
    use GenericEventValidationRules;

    public function __construct(BaseEventHeaders $headers, BaseEventBody $body)
    {
        $this->headers = $headers;
        $this->body = $body;
    }

    public function getHeaders(): BaseEventHeaders
    {
        return $this->headers;
    }

    public function getBody(): BaseEventBody
    {
        return $this->body;
    }

    public static function validate(array $payload, array $rules = null): void
    {
        $errors = [];

        if (!isset($payload['headers'])) {
            $errors['headers'][] = "Event payload must have 'headers' property";
        }

        if (!isset($payload['body'])) {
            $errors['body'][] = "Event payload must have 'body' property";
        }

        if (!empty($errors)) {
            throw new ValidationError($errors);
        }
    }

    public static function validateAndParse(array $payload): self
    {
        self::validate($payload);

        return new self(
            BaseEventHeaders::validateAndParse($payload),
            BaseEventBody::validateAndParse($payload),
        );
    }
}

<?php

namespace MVF\Servicer\BaseEvent;

use MVF\Servicer\ArraySerializer;
use MVF\Servicer\Events\GenericBody;
use MVF\Servicer\Exceptions\ValidationError;

class BaseEventBody extends GenericBody
{
    use ArraySerializer;

    private array $body;

    public function __construct(array $body)
    {
        $this->body = $body;
    }

    public function toArray(): array
    {
        return $this->body;
    }

    public static function validate(array $payload, array $rules = null): void
    {
        $errors = [];

        if (!is_array($payload['body'])) {
            $errors['body'][] = "Event payload 'body' must be an array";
        }

        if (!empty($errors)) {
            throw new ValidationError($errors);
        }
    }

    public static function validateAndParse(array $payload): self
    {
        self::validate($payload);

        return new self($payload['body']);
    }
}

<?php

namespace MVF\Servicer\BaseEvent;

use MVF\Servicer\ArraySerializer;
use MVF\Servicer\Constants\Timestamp;
use MVF\Servicer\Events\DefaultValidationRules\Valitron\EventHeaderValidationRules;
use MVF\Servicer\Events\GenericHeaders;
use MVF\Servicer\Exceptions\ValidationError;

class BaseEventHeaders extends GenericHeaders
{
    use ArraySerializer, EventHeaderValidationRules;

    public static function validate(array $payload, array $rules = null): void
    {
        $errors = [];

        if (!is_array($payload['headers'])) {
            $errors['headers'][] = "Event payload 'headers' must be an array";
        }

        if (!isset($payload['headers']['event'])) {
            $errors['headers.event'][] = "Event payload must have 'headers.event' property";
        }

        if (!isset($payload['headers']['version'])) {
            $errors['headers.version'][] = "Event payload must have 'headers.version' property";
        } elseif (is_string($payload['headers']['version']) && strlen($payload['headers']['version']) === 0) {
            $errors['headers.version'][] = "Event payload 'headers.version' must be non empty string";
        }

        if (!isset($payload['headers']['identifier'])) {
            $errors['headers.identifier'][] = "Event payload must have 'headers.identifier' property";
        } elseif (is_string($payload['headers']['identifier']) && strlen($payload['headers']['identifier']) === 0) {
            $errors['headers.identifier'][] = "Event payload 'headers.identifier' must be non empty string";
        }

        if (!isset($payload['headers']['createdAt'])) {
            $errors['headers.createdAt'][] = "Event payload must have 'headers.createdAt' property";
        } elseif (is_integer($payload['headers']['createdAt']) && $payload['headers']['createdAt'] < Timestamp::START_OF_YEAR_2000) {
            $errors['headers.createdAt'][] = "Event payload 'headers.createdAt' must be a unix timestamp post year 2000";
        }

        if (!empty($errors)) {
            throw new ValidationError($errors);
        }
    }

    public static function validateAndParse(array $payload): self
    {
        self::validate($payload);

        return new static(
            $payload['headers']['event'],
            $payload['headers']['version'],
            $payload['headers']['identifier'],
            $payload['headers']['createdAt'],
        );
    }
}

<?php

namespace MVF\Servicer;

class EventsSource
{
    protected static function source(string $url, array $events): array
    {
        $output = [];
        foreach ($events as $event => $handler) {
            $output["$url->$event"] = $handler;
        }

        return $output;
    }
}

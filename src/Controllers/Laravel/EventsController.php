<?php

namespace MVF\Servicer\Controllers\Laravel;

use GuzzleHttp\Utils;
use MVF\Servicer\Contracts\Logger;
use MVF\Servicer\Receiver;
use Psr\Container\ContainerInterface;

class EventsController
{
    private ContainerInterface $container;
    private Logger $logger;
    private string $content;

    public function __construct(ContainerInterface $container, Logger $logger, string $content)
    {
        $this->container = $container;
        $this->logger = $logger;
        $this->content = $content;
    }

    public function create()
    {
        $payload = Utils::jsonDecode($this->content, true)['data'];

        $headers = $payload['headers'];
        $body = $payload['body'];

        $receiver = new Receiver($this->container, $this->logger, $headers['context']['queueUrl']);
        $receiver->handle($headers, $body);

        return Utils::jsonEncode([
            'data' => [],
        ]);
    }
}

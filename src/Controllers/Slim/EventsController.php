<?php

namespace MVF\Servicer\Controllers\Slim;

use GuzzleHttp\Utils;
use MVF\Servicer\Contracts\Logger;
use MVF\Servicer\Receiver;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class EventsController
{
    private ContainerInterface $container;
    private Logger $logger;

    public function __construct(ContainerInterface $container, Logger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function create(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $payload = Utils::jsonDecode($request->getBody()->getContents(), true)['data'];

        $headers = $payload['headers'];
        $body = $payload['body'];

        $receiver = new Receiver($this->container, $this->logger, $headers['context']['queueUrl']);
        $receiver->handle($headers, $body);

        $response->getBody()->write(Utils::jsonEncode([
            'data' => [],
        ]));

        return $response;
    }
}

# Servicer PHP v1

> Various helpers that can be used in PHP to simplify event creation and consumption.

This package is meant to be used together with [event-consumer](https://bitbucket.org/mvfglobal/event-consumer/src/master/) re-usable service. 

# Getting started

In this section you will see how to set up a PHP application to consume and publish events.

## Ingress (Event Consumer)

In order to consume events in a PHP application you will need to use [event-consumer](https://bitbucket.org/mvfglobal/event-consumer/src/master/) re-usable service. This service is a node based application which is needed due to the limitations of PHP, long story short, php is not great for long-running tasks, when a PHP process runs for a long time without any restarts there tends to be resource leaks and various other issues that make PHP based consumers unreliable. 

![](./doc/images/event-ingress.png)

In the diagram above you can see what this architecture looks like. The `event-consumer` service reads messages (events) stored in the SQS queues and sends each consumed message to the `events` endpoint on your webserver.

Posting event messages over network in this way is a bit strange but this allows us to achieve 2 things that are normally really hard in PHP.

* This puts a webserver (either apache or nginx) in between the `event-consumer` and our `EventHandler`, this is the easiest and the most reliable way to achieve "concurrency" in PHP. __NOTE__ There are alternatives approaches e.g. `ReactPHP`, however, they are not commonly used in the PHP community.

* This avoids long-running processes, each message is handled by a different instance of a PHP process avoiding various issues long-running processes have in PHP. 

In your application you will need two things:

* `Controller` where the `events` route will be handles

* `Psr\Container\ContainerInterface` compatible container, this is where you will register any dependencies required by the controller and your `EventHandlers`

Learn how to set up:

* [Events Controller](doc/EventsController/README.md)

* [Event Handlers](doc/EventHandlers/README.md)
    
    * [Run Action](doc/EventHandlers/RunAction.md) - this event handler can be used to run object that implement `Action` interface
    * [Run Redirect](doc/EventHandlers/RunRedirect.md) - this event handler can be used to transform event to another event which can then be processed by a different event handler.

## Egress (Event Publishing)


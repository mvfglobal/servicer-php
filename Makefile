.DEFAULT_GOAL := up

INIT := $(shell ls ./env | grep -q docker || ./scripts/create-docker-env.sh)
include ./env/docker

.PHONY: up
up:
	$(MAKE) down
	docker-compose up -d

.PHONY: down
down:
	docker-compose down --remove-orphans

.PHONY: build
build:
	docker-compose build
	$(MAKE) up

.PHONY: logs
logs:
	docker-compose logs -f --tail=100

.PHONY: shell
shell:
	docker exec -u www -it servicer-php sh

.PHONY: test
test:
	docker exec -u www -it servicer-php sh -c "..."

.PHONY: setup
setup:
	scripts/check-project.sh
	$(MAKE) down
	rm -fr ./vendor
	$(MAKE) up
	docker exec -u www -it servicer-php composer install

# Run Redirect

`RunRedirect` is a helper event handler that allows you to trigger a different event, it works by creating an event transformer

## Event Transformer

make a class that implements `EventTransformer` e.g.

```php
namespace Events\PaymentPaid\Transformers;

...
use MVF\Servicer\Contracts\EventTransformer;
use MVF\Servicer\Contracts\QueueTransformer;

class PaymentPaidToNotificationCreated implements EventTransformer
{
    public function __invoke(array $payload): AbbeyNotificationCreated
    {
        $payload = PaymentPaid::validateAndParse($payload);
        $orderId = BaseModel::query()
            ->from(PaymentTable::getTableName())
            ->where(PaymentTable::externalPaymentId(), $payload->getBody()->getId())
            ->firstOrFail([PaymentTable::orderId()])
            ->{PaymentTable::orderId()};

        return new AbbeyNotificationCreated(
            new AbbeyNotificationCreatedHeaders('1', sprintf(
                '%s:%s',
                NotificationType::ORDER_PAID,
                $orderId,
            )),
            new AbbeyNotificationCreatedBody(
                NotificationType::ORDER_PAID,
                $orderId,
            ),
        );
    }
}
```

This class must implement `__invoke` function which takes the original event as `$payload` argument and returns a new event you want to trigger. The above example transform `PaymentPaid` event to `AbbeyNotificationCreated` event.

__IMPORTANT!__ with the above implementation `AbbeyNotificationCreated` event handler must be in the same `EventSource`.

## Queue Transformer

If you want you transformed event to be processed by a different `EventSource` you will also have to implement `QueueTransformer`, e.g.

```php
namespace Events\PaymentPaid\Transformers;

...
use MVF\Servicer\Contracts\EventTransformer;
use MVF\Servicer\Contracts\QueueTransformer;

class PaymentPaidToNotificationCreated implements EventTransformer, QueueTransformer
{
    public function __invoke(array $payload): AbbeyNotificationCreated
    {
        ...
    }

    public function getQueueOverride(): string
    {
        return 'https://sqs.eu-west-1.amazonaws.com/801723641363/abbey-internal-events';
    }
}
```

# IdempotencyChecker (example)

```sql
-- auto-generated definition
create table idempotent_entities
(
    identifier              varchar(255)                       not null
        primary key,
    previously_generated_at datetime                           null,
    generated_at            datetime                           not null,
    created_at              datetime default CURRENT_TIMESTAMP not null
) collate = utf8_unicode_ci;
```

```php
<?php

namespace Custom\EventManager\EventHandlerDecorators;

use App\Clients\EloquentClient;
use App\Eloquent\BaseModel;
use Carbon\Carbon;
use Constants\Database;
use Custom\Concurrency\OutdatedOperation;
use Custom\Logging\GlobalLogger;
use Custom\Table\Schemas\Abbey\IdempotentEntityTable;
use MVF\Servicer\EventHandlerDecorator;
use Throwable;

class IdempotencyChecker extends EventHandlerDecorator
{
    private ?string $prefix;

    public function __construct(string $prefix = null)
    {
        $this->prefix = $prefix;
    }

    /**
     * @throws Throwable
     */
    public function consume(array $payload)
    {
        try {
            $event = $payload['headers']['event'];
            return EloquentClient::connection(Database::WRITE)->transaction($this->deduplicate($payload));
        } catch (OutdatedOperation $exception) {
            GlobalLogger::instance()->warn("$event event is outdated and will be ignored", [
                'headers' => $payload['headers'],
            ]);

            return null;
        }
    }

    private function deduplicate(array $payload): callable
    {
        return function () use ($payload) {
            $prefix = $this->prefix ?? 'event';
            $event = $payload['headers']['event'];
            $identifier = sprintf('%s:%s:%s', $prefix, $event, $payload['headers']['identifier']);
            $generatedAt = Carbon::createFromTimestamp($payload['headers']['createdAt']);

            $query = strtr(
                '
                    INSERT INTO %table (%identifier, %generatedAt, %createdAt) VALUES (?, ?, ?)
                    ON DUPLICATE KEY UPDATE 
                      %previousGeneratedAt = %generatedAt,
                      %generatedAt = IF(%generatedAt < VALUES(%generatedAt), VALUES(%generatedAt), %generatedAt);
                ',
                [
                    '%table' => IdempotentEntityTable::getTableName(),
                    '%identifier' => IdempotentEntityTable::identifier(),
                    '%generatedAt' => IdempotentEntityTable::generatedAt(),
                    '%previousGeneratedAt' => IdempotentEntityTable::previousGeneratedAt(),
                    '%createdAt' => IdempotentEntityTable::createdAt(),
                ]
            );

            EloquentClient::connection(Database::WRITE)->statement($query, [$identifier, $generatedAt, $generatedAt]);
            $entity = BaseModel::on(Database::WRITE)
                ->from(IdempotentEntityTable::getTableName())
                ->where(IdempotentEntityTable::identifier(), $identifier)
                ->first();

            $generatedAt = Carbon::parse($entity->{IdempotentEntityTable::generatedAt()})->timestamp;
            $previousGeneratedAt = isset($entity->{IdempotentEntityTable::previousGeneratedAt()})
                ? Carbon::parse($entity->{IdempotentEntityTable::previousGeneratedAt()})->timestamp
                : null;

            if (isset($previousGeneratedAt) === false || $previousGeneratedAt < $generatedAt) {
                return $this->decorator->consume($payload);
            } else {
                throw new OutdatedOperation('outdated action detected, operation terminated');
            }
        };
    }
}
```
# Event Handlers

This section will show you how to create and bind event handlers to your containers.

## Event Source

First, you want to create an event source for your SQS queue.

Say we have https://sqs.eu-west-1.amazonaws.com/000000000000/prd7-abbey-subsription-events SQS queue, we can create an `EventSource` for it

```php
namespace App\EventSources;

use MVF\Servicer\EventsSource;

class SubscriptionEventSource extends EventsSource
{
    public static function handlers(): array
    {
        return self::source('https://sqs.eu-west-1.amazonaws.com/000000000000/prd7-abbey-subsription-events', [
            // TODO: Define EventHandlers
        ]);
    }
}
```

You may also want to use a different queue based on the environment, lets say you have the following 2 SQS queues

* https://sqs.eu-west-1.amazonaws.com/000000000000/prd7-abbey-subsription-events

* https://sqs.eu-west-1.amazonaws.com/000000000000/stg7-abbey-subsription-events

These are almost identical except for the prefix, one approach could be to create template url and store it in the environment of the app, e.g.

```env
SQS_URL_TEMPLATE=https://sqs.eu-west-1.amazonaws.com/000000000000/stg7-abbey-%s-events
```

note the `%s` in the value of `SQS_URL_TEMPLATE`, then you could use `sprintf`

```php
    public static function handlers(): array
    {
        $queueUrl = sprintf(getenv('SQS_URL_TEMPLATE'), 'subscriptions');

        return self::source($queueUrl, [
            // TODO: Define EventHandlers
        ]);
    }
```

## Naive EventHandler

First, create a class that implements `EventHandler`

```php
<?php

namespace App\Actions\CreateSubscription;

use MVF\Servicer\Contracts\EventHandler;

class CreateSubscription implements EventHandler
{
    public function consume(array $payload)
    {
        // TODO: create subscription...
    }
}
```

Then, add event name constant and map it to an EventHandler

```php
<?php

namespace App\EventSources;

use App\Actions\CreateSubscription\CreateSubscription;
use MVF\Servicer\Contracts\EventHandler;
use MVF\Servicer\EventsSource;
use MVF\Servicer\RunAction;

class SubscriptionEventSource extends EventsSource
{
    const SUBSCRIPTION_CREATED = 'SUBSCRIPTION_CREATED';

    public static function handlers(): array
    {
        $queueUrl = sprintf(getenv('SQS_URL_TEMPLATE'), 'subscriptions');

        return self::source($queueUrl, [
            self::SUBSCRIPTION_CREATED => fn() => new CreateSubscription(),
        ]);
    }
}
```

`CreateSubscription` will run every time `SUBSCRIPTION_CREATED` event is consumed, this is a functional but naive approach, the action is triggered without any consideration for idempotency. __IMPORTANT!__ SQS ensures 1 or more delivery, it does not ensure that only 1 message will be sent to the consumer, and it also does not ensure that messages will be consumed in the right order.

## Better EventHandler

Create a decorator preset class

```php
<?php
/**
 * All decorator presets will be executed in the order that they are defined.
 *
 * IMPORTANT! changing the order of decorators can and most likely will change behaviour
 */
class DecoratorPreset
{
    public static function core(string $name = null): array
    {
        return [
            new ErrorLogging($name),
            new IdempotencyChecker($name),
        ];
    }
}
```

* ErrorLogging [example](./ErrorLogging.md)
* IdempotencyChecker [example](./IdempotencyChecker.md)

Then use your decorator preset together with `MVF\Servicer\DecoratedConsumer` and `MVF\Servicer\RunAction` like this

```php
use MVF\Servicer\DecoratedConsumer;
use MVF\Servicer\RunAction;
use App\Actions\CreateSubscription\CreateSubscription;

class SubscriptionEventSource extends EventsSource
{
    const SUBSCRIPTION_CREATED = 'SUBSCRIPTION_CREATED';

    public static function handlers(): array
    {
        $queueUrl = sprintf(getenv('SQS_URL_TEMPLATE'), 'subscriptions');

        return self::source($queueUrl, [
            self::SUBSCRIPTION_CREATED => fn(): EventHandler => new DecoratedConsumer(
                DecoratorPreset::core(),
                new CreateSubscription(),
            ),
        ]);
    }
}
```

Assuming your decorators are implemented correctly this will ensure handler idempotency.

## Multiple handlers per event

It is possible to have multiple event handlers for each event

```php
public static function handlers(): array
{
    $queueUrl = sprintf(getenv('SQS_URL_TEMPLATE'), 'subscriptions');

    return self::source($queueUrl, [
        self::SUBSCRIPTION_CREATED => [
            'a' => fn(): EventHandler => new DecoratedConsumer(
                DecoratorPreset::core(),
                new A(),
            ),
            'b' => fn(): EventHandler => new DecoratedConsumer(
                DecoratorPreset::core(),
                new B(),
            ),
            ...
        ],
    ]);
}
```

__IMPORTANT!__ the value of the array keys will be appended to the `headers['identifier']` property to make idempotency decorator treat each handler as different events, without this idempotency checker would break. To be explicit this is also the reason your keys must be unique.

## Add EventHandlers to DI Container

This might differ slightly depending on your container implementation, e.g. in laravel this would look something like this

```php
use Illuminate\Foundation\Application;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [];

    public function __construct(Application $app)
    {
        foreach (SubscriptionEventSource::handlers() as $key => $handler) {
            $app->bind($key, $handler);
        }

        parent::__construct($app);
    }
}
```

This works because handlers return an array that looks like this

```php
[
    '{queueUrl}->{event}' => fn(): EventHandler => ...,
    ...
]
```

Setup with other DI containers may look a bit different but in all cases you want the `{queueUrl}->{event}` to be the key in the DI container and `fn(): EventHandler => ...` to be the concrete implementation returned when `{queueUrl}->{event}` is requested.

# ErrorLogging (example)

```php
<?php

namespace Custom\EventManager\EventHandlerDecorators;

use Custom\Logging\GlobalLogger;
use MVF\Servicer\EventHandlerDecorator;
use Throwable;

class ErrorLogging extends EventHandlerDecorator
{
    private ?string $name;

    public function __construct(string $name = null)
    {
        $this->name = $name;
    }

    /**
     * @throws Throwable
     */
    public function consume(array $payload)
    {
        if (isset($name)) {
            GlobalLogger::instance()->setGroupName($this->name);
        }

        try {
            return $this->decorator->consume($payload);
        } catch (Throwable $exception) {
            GlobalLogger::instance()->append($exception);
            throw $exception;
        }
    }
}
```
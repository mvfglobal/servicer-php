# Run Action

`RunAction` is a helper event handler that allows you to easily trigger any class that implements `Action` interface, e.g.

```php
<?php

namespace App\Actions\CreateSubscription;

use MVF\Servicer\Contracts\Action;

class CreateSubscription implements Action
{
    public function handler(array $request): array
    {
        // TODO: create subscription...

        return [];
    }
}
```

Then, use `RunAction` event handler in your event source

```php
<?php

namespace App\EventSources;

use App\Actions\CreateSubscription\CreateSubscription;
use MVF\Servicer\Contracts\EventHandler;
use MVF\Servicer\EventsSource;
use MVF\Servicer\RunAction;

class SubscriptionEventSource extends EventsSource
{
    const SUBSCRIPTION_CREATED = 'SUBSCRIPTION_CREATED';

    public static function handlers(): array
    {
        $queueUrl = sprintf(getenv('SQS_URL_TEMPLATE'), 'subscriptions');

        return self::source($queueUrl, [
            self::SUBSCRIPTION_CREATED => function (): EventHandler {
                return new RunAction(new CreateSubscription());
            },
        ]);
    }
}
```

# EventTransformerMustReturnGenericEvent

This error is thrown if your event transformer does not return an object that extends `MVF\Servicer\Events\GenericEvent`. For example, you might have something like

```php
class ClientLeadsDistributedToProspectStatusChanged implements EventTransformer
{
    public function __invoke(array $payload): ProspectStatusUpdated
    {
        return return new ProspectStatusUpdated();
    }
}
```

where `ProspectStatusUpdated` is

```php
use MVF\Servicer\Contracts\ArraySerializable;

class ProspectStatusUpdated implements ArraySerializable
{
    public function toArray(): array
    {
        return [];
    }
}
```

to fix this `ProspectStatusUpdated` must extend `MVF\Servicer\Events\GenericEvent`, you should have something like this

```php
use Custom\Validators\Validator;
use MVF\Servicer\Events\DefaultValidationRules\Valitron\GenericEventValidationRules;
use MVF\Servicer\Events\GenericEvent;

class ProspectStatusUpdated extends GenericEvent
{
    use Validator, GenericEventValidationRules;

    public function __construct(ProspectStatusUpdatedHeaders $headers, ProspectStatusUpdatedBody $body)
    {
        $this->headers = $headers;
        $this->body = $body;
    }

    public function getHeaders(): ProspectStatusUpdatedHeaders
    {
        return $this->headers;
    }

    public function getBody(): ProspectStatusUpdatedBody
    {
        return $this->body;
    }

    public static function validateAndParse(array $payload): self
    {
        self::validate($payload, self::defaultValidationRules());

        return new self(
            ProspectStatusUpdatedHeaders::validateAndParse($payload),
            ProspectStatusUpdatedBody::validateAndParse($payload),
        );
    }
}
```

If you are not sure how event objects should be structured, read getting started docs [here](../../README.md).
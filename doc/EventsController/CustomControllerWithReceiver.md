# Custom Controller With Receiver

You can use `MVF\Servicer\Receiver` in your own custom controller or route handler.

## Receiver

```php
use MVF\Servicer\Receiver;

$receiver = new Receiver($container, $logger, $headers['context']['queueUrl']);
$receiver->handle($headers, $body);
```

the `Receiver` class takes

1. Any container implementation compatible with `Psr\Container\ContainerInterface`
2. Any logger implementation compatible with `MVF\Servicer\Contracts\Logger`
3. Queue url, each queue is treated as a separate event source, this is needed for cases where the event name is not unique.

The `handle(...)` method called on the `Receiver` will look for `EventHandler` using `"{queueUrl}->{event}"` pattern, e.g.

* if `$headers['context']['queueUrl']` is `subscription-events`, and

* if `$headers['event']` is `SUBSCRIPTION_CREATED`

* then `Receiver` will attempt to get `EventHandler` with `$container->get('subscription-events->SUBSCRIPTION_CREATED')`

This means that your event handlers must be registered in your container, more on this in the [Event Handlers](../EventHandlers/README.md) section.

## Container

Your application will need to have some kind of container technology that is compatible with `Psr\Container\ContainerInterface`, if your app currently doesn't use containers you can always install and set one up, e.g. [PHP DI](https://php-di.org/).

## Logger

This you will just have to implement yourself, create any class e.g.

```php
<?php

namespace Custom\Logging;

use MVF\Servicer\Contracts\Logger;

class EventsLogger implements Logger
{
    public function info(string $message, array $meta = []): void
    {
        // TODO: Implement info() method.
    }

    public function warn(string $message, array $meta = []): void
    {
        // TODO: Implement warn() method.
    }
}
```

then implement `info` and `warn` function using whatever logger you have available in your application.

After your logger is implemented don't forget to register it in your container, e.g. if you are using laravel

```php
use Custom\Logging\EventsLogger;

$app->bind(Logger::class, fn() => new EventsLogger());
```

## Request Body

The `event-consumer` service will post message data in the body of the request, the message itself has the following format.

```json
{
    "data": {
        "headers": {
            "version": "1",
            "event": "SUBSCRIPTION_CREATED",
            "createdAt": 946684800, // Start of year 2000 or greater
            "context": {
                "queueUrl": "subscriptions-topic" // the SQS queue url where the event came from
            },
            ...
        },
        "body": {
            ...
        }
    }
}
```

##  Setup

```php
$container = ... // get an instance of your container
$logger = ... // get an instance of your logger
$content = ... // load request body as string into $content variable

$payload = Utils::jsonDecode($content, true)['data'];

$headers = $payload['headers'];
$body = $payload['body'];

$receiver = new Receiver($container, $logger, $headers['context']['queueUrl']);
$receiver->handle($headers, $body);

// if there was no error you must make sure your request completes with non error status
return Utils::jsonEncode([
    'data' => [],
]);
```

__IMPORTANT!__ in the event your request completes with an error code `event-consumer` will assume the message was not handled and therefore will not delete the message from the queue, it is crucial that your code and your event handlers, handle errors correctly.

* The message will be retried after the SQS visibility timeout, more on this can be read in AWS documentation [here](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html)
* If failures continue, the failing message will be sent to the dead-letter queue, more on this can be read in AWS documentation [here](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html)

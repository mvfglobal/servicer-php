# Laravel Events Controller

Setup events route

```php
use Illuminate\Support\Facades\Route;
use MVF\Servicer\Controllers\Laravel\EventsController;

Route::post('events', [EventsController::class, 'create']);
```

Setup required binding

```php
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;
use MVF\Servicer\Contracts\Logger;
use MVF\Servicer\Controllers\Laravel\EventsController;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    public function __construct(Application $app)
    {
        $app->bind(Logger::class, fn() => new LogCollection());
        $app->when(EventsController::class)
            ->needs('$content')
            ->give(fn() => $app->make(Request::class)->getContent());

        parent::__construct($app);
    }
}
```

You can create a custom class e.g. `LogCollection` and then implement `MVF\Servicer\Contracts\Logger` interface with required functions yourself.

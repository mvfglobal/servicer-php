# Custom Controller Without Receiver (Not Recommended)

with this approach you will lose the following:

* RunAction event handler - Useful when an action needs to be executed

* RunRedirect event handler - Useful when you want to redirect event to a different event handler

* EventHandlerDecorator and DecoratedConsumer - This helps with the implementation of `event deduplication`, `logging`

* Logging - Default event consumption logs

* Multiple handler per event - this allows you to run multiple event handlers per event

You can create your own Events Controller implementation, everything you need will be posted to your events route by the `event-consumer` in the request body, which has the following structure.

```php
{
    "data": {
        "headers": {
            "version": "1",
            "event": "SUBSCRIPTION_CREATED",
            "createdAt": 946684800, // Start of year 2000 or greater
            "context": {
                "queueUrl": "subscriptions-topic" // the SQS queue url where the event came from
            },
            ...
        },
        "body": {
            ...
        }
    }
}
```

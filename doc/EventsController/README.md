# Events Controller

The exact details for how events controller must be implemented will differ slightly depending on the framework. Some framework controllers are already implemented by the package, if controller is not already provided then you can also implement your own. The following links show how to set up provided controllers in their respective frameworks.

* [Laravel Events Controller](./LaravelEventsController.md)
* [Slim Events Controller](./SlimEventsController.md)

There are 2 alternative approaches

* [Custom Controller With Receiver](./CustomControllerWithReceiver.md)
* [Custom Controller Without Receiver](./CustomControllerWithoutReceiver.md)

# Slim Events Controller

Create application instance with container and required bindings

```php
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use MVF\Servicer\Contracts\Logger;
use DI\Bridge\Slim\Bridge;

$container = null;
$builder = new ContainerBuilder();
$builder->addDefinitions([
    ContainerInterface::class => fn() => $container,
    Logger::class => fn() => new LogCollection(),
]);

$container = $builder->build();
$app = Bridge::create($container);
```

You can create a custom class e.g. `LogCollection` and then implement `MVF\Servicer\Contracts\Logger` interface with required functions yourself.

Setup events route

```php
use MVF\Servicer\Controllers\Slim\EventsController;

$app->post('/events', [EventsController::class, 'create']);
```

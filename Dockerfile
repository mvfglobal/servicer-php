FROM alpine:3.15

ARG BUILD_GITHUB_ACCESS_TOKEN
ARG OS_UID
ARG OS_GID

RUN apk add --no-cache --update bash curl shadow git composer php8-cli php8-redis
RUN ln -s /usr/bin/php8 /usr/bin/php

USER root
RUN /usr/sbin/groupadd -g ${OS_GID} www \
  && /usr/sbin/useradd -s /bin/bash --create-home -m -g ${OS_GID} -u ${OS_UID} www \
  && mkdir /app && chown www:www /app

USER www
WORKDIR /app

COPY --chown=www composer.* ./

RUN composer config --global --auth github-oauth.github.com ${BUILD_GITHUB_ACCESS_TOKEN} \
    && composer install -n --no-autoloader --no-scripts --no-progress

COPY --chown=www . .

RUN composer dump-autoload -o
